# README #

This is a Magic Set Editor 2 extension for generating .xml files suitable to use in Cockatrice.

### How to use ###

* Clone the directory into your _Magic Set Editor 2/data_ folder
* Alternatively, unpack the .zip file from [dropbox](https://www.dropbox.com/s/tr2so8ubsycfcpz/magic-cockatrice.mse-export-template.zip?dl=0) and move it to the _Magic Set Editor 2/data_ folder
* Launch Magic Set Editor 2 after deploying the _magic-cokcatrice.mse-export-template_ folder
* In MSE2, go to File -> Export -> HTML and click Cockatrice


### Unimplemented features ###

* Split cards
* Flip cards
* Dual faced cards
* Level-up cards
* Esoteric needs _(custom colors or templates, non-obvious references, font styling, etc)_